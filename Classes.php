<?php
    Class student {
        private $studid;
        private $firstname;
        private $lastname;
        private $email;
        private $studentprog;
        
        function getStudID() {
            return $this->studid;
        }
        
        function getFirstName() {
            return $this->firstname;
        }
        
        function getLastName() {
            return $this->lastname;
        }
        
        function getEmail() {
            return $this->email;
        }
        
        function getStudentprog() {
            return $this->studentprog;
        }

        
        function setStudID($studid) {
            if(!preg_match("/[a-zA-Z0-9_.-]/", $studid)) {
                return false;
            } else {
                $this->studid=$studid;
                return true;
            }
        }
        
        function setFirstName($firstname) {
            if(!preg_match("/[a-zA-Z. ]/",$firstname)) {
                return false;
            } else {
                $this->firstname=$firstname;
                return true;
            }
        }
        
        function setLastName($lastname) {
            if(!preg_match("/[a-zA-Z. ]/",$lastname)) {
                return false;
            } else {
                $this->lastname=$lastname;
                return true;
            }
        }
        
                
        function setEmail($email) {
            if(!preg_match("/^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/", $email)) {
                return false;
            } else {
                $this->email=$email;
                return true;
            }
        }
 
        function setStudentProg($studentprog) {
            if(!preg_match("/[a-zA-Z. ]/",$studentprog)) {
                return false;
            } else {
                $this->studentprog=$studentprog;
                return true;
            }
        }
    }
    
        Class StudyProgram {
        private $studyId;
        private $studyName;
        
        function getStudyId() {
            return $this->studyId;
        }
        
        function getStudyName() {
            return $this->studyName;
        }
        
         function setStudyId($studyId) {
            if(!preg_match("/[0-9][a-zA-Z. ]{11}/", $studyId)) {
                return false;
            } else {
                $this->stuyId=$studyId;
                return true;
            }
        }
        
        function setStudyName($studyName) {
            if(!preg_match("/[a-zA-Z. ]/", $studyName)) {
                return false;
            } else {
                $this->stuyName=$studyName;
                return true;
            }
        }
        
        }